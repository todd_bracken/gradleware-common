# Installation:

## Add the following to your project's build file..
```gradle
task installCommonGradleArchiver << {
	// Clean
	if(file('gradle-common').exists()) {
		delete 'gradle-common'
	}
	assert !file('gradle-common').exists()
	
	// Download Latest
	def commonGradleGitRepo = 'http://asis.dcma.mil/r/integration/cd/gradle-scripts-common.git'
	['git', 'clone', commonGradleGitRepo, 'gradle-common'].execute().waitForProcessOutput(System.out, System.out)
		
	if(file('settings.gradle').exists()) {
		println "**NOTE**  After task completes, update the 'settings.gradle' file in your project's root..."
		println "**----**  Append the following to your existing 'include' (inside the quotes): \",':gradle-common:archives'\""
	}else {
		def settingsFile = new File("settings.gradle")
		settingsFile << "include ':gradle-common:archives'"
	}	
}
```

## Run the installer...
```gradle
$gradle installCommonGradleArchiver
```

# Usage (assumes that all installation/configuration already completed):
```gradle
// Build / archive:
$gradle build
$gradle jar

// Get current version info
$gradle :getCurrVersionInfo

// Upload archive to Artifactory (eg. snapshot)
$gradle clean getNextVersionInfo build uploadArtifact -DreleaseType=release -DartifactoryPwd=<pwd>
```

### Example Usage/Output
```
$ gradle clean jar uploadArtifact -DreleaseType=r -DartifactoryPwd=*******
:clean
:compileJava
:processResources UP-TO-DATE
:classes
:jar
[RELEASE] Gradle Release Plugin version 16.4
[RELEASE] Preparing Release Version Info for Project: SSO Broker (Original Flavor)
[RELEASE UTIL] On Release Branch, Setting build as RELEASE/PATCH...
[RELEASE] Release Type: RELEASE (PATCH)
[RELEASE] Checking for 'Flashback' release (i.e. has a newer version already been released)
[RELEASE (GIT)] Creating Git Tag (patches/16.1.1-PATCH) on commit (7d97a178f979cbeb9083061e0f8e99b0f1ecc26a)...
[RELEASE (GIT)] Pushing Git Tag (patches/16.1.1-PATCH) to 'origin'...
[RELEASE] LAST RELEASE: 16.1.1-PATCH
[RELEASE] CURRENT BUILD:   16.1.1-PATCH
:gradle-common:archives:uploadArtifact
[ARCHIVE] Initializing Archiver
[ARCHIVE] Archiver started at Fri Sep 30 10:27:10 EDT 2016
[ARCHIVE] Uploading archive (please wait...)
{
  "repo" : "libs-gots-release",
  "path" : "/mil/dcma/common/sso-broker/16.1.1-PATCH/sso-broker-16.1.1-PATCH.jar",
  "created" : "2016-09-30T10:26:35.442-04:00",
  "createdBy" : "admin",
  "downloadUri" : "http://artifactrepo.dcma.mil:8080/artifactory/libs-gots-release/mil/dcma/common/sso-broker/16.1.1-PATCH/sso-broke
r-16.1.1-PATCH.jar",
  "mimeType" : "application/java-archive",
  "size" : "29285",
  "checksums" : {
    "sha1" : "efac4990d726fa4b245950638fd2b6aedf9c5d89",
    "md5" : "69f5aa80c6241c4677e73eccf947e3c3"
  },
  "originalChecksums" : {
  },
  "uri" : "http://artifactrepo.dcma.mil:8080/artifactory/libs-gots-release/mil/dcma/common/sso-broker/16.1.1-PATCH/sso-broker-16.1.1
-PATCH.jar"
}

[ARCHIVE] Archive upload information now available in the following file: artifact-repo-info.json
[ARCHIVE] Archive Uploaded using the following info:
PROJECT.GROUP           > mil.dcma.common
PROJECT.ID              > sso-broker
PROJECT.VER             > 16.1.1-PATCH
PROJECT.BUILDTYPE       > RELEASE
POM.VER                 > 16.1.1-PATCH
POM.GroupID             > mil.dcma.common
POM.ArtifactID          > sso-broker
ARTIFACTREPO.URL        > http://artifactrepo.dcma.mil:8080/artifactory/libs-gots-release
ARTIFACT.URL            > http://artifactrepo.dcma.mil:8080/artifactory/libs-gots-release/mil/dcma/common/sso-broker/16.1.1-PATCH/ss
o-broker-16.1.1-PATCH.jar;commitHash=7d97a178f979cbeb9083061e0f8e99b0f1ecc26a

BUILD SUCCESSFUL

Total time: 7.039 secs
```



